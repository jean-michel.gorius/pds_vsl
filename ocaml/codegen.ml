open ASD
open Llvm
open Utils
open List
open SymbolTable

let llvm_type_of_asd_type t =
  match t with
  | Type_Int -> LLVM_Type_Int
  | Type_Void -> LLVM_Type_Void
  | _ -> failwith ("No type corresponding to array exist in LLVM")

(* main function. return only a string: the generated code *)
let rec ir_of_ast p =
  (* this header describe to LLVM the target
   * and declare the external function printf
   *)
  let head = "; Target\n" ^
             "target triple = \"x86_64-unknown-linux-gnu\"\n" ^
             "; External declaration of the printf function\n" ^
             "declare i32 @printf(i8* noalias nocapture, ...)\n" ^
             "declare i32 @scanf(i8* noalias nocapture, ...)\n" ^
             "\n; Actual code begins\n\n"

  in let (ir, _) = ir_of_functional_elem_list (empty_ir, []) p

  (* generates the final string *)
  in head ^
  (string_of_ir ir)

(* All main code generation functions take the current IR and a scope,
 * append header and/or code to the IR, and/or change the scope
 * They return the new pair (ir, scope)
 * This is convenient with List.fold_left
 *
 * They can return other stuff (synthesized attributes)
 * They can take extra arguments (inherited attributes)
 *)

(* returns the regular pair, plus the pair of the name of the result and its type *)
and ir_of_expression (ir, scope) =
  (* function to generate all binop operations *)
  let aux op t (l, r) =
    (* generation of left operand computation. *)
    let (left_ir, left_scope), (lresult_name, lresult_type)
      = ir_of_expression (ir, scope) l
    (* generation of right operand computation. *)
    (* it appends code to the previous code generated *)
    in let (right_ir, right_scope), (rresult_name, rresult_type)
      = ir_of_expression (left_ir, left_scope) r

    and result = newtmp ()

    in let _ = if lresult_type <> rresult_type || t <> rresult_type then failwith "Type error"

    in let code = Binop {
      lvalue_name = result;
      lvalue_type = t;
      op = op;
      left = lresult_name;
      right = rresult_name;
    }

    in
    ((append_instr right_ir code, scope), (result, t))

  in function
    | AddExpression (l, r) -> aux "add" LLVM_Type_Int (l, r)
    | SubstExpression (l, r) -> aux "sub" LLVM_Type_Int (l, r)
    | MulExpression (l, r) -> aux "mul" LLVM_Type_Int (l, r)
    | DivExpression (l, r) -> aux "div" LLVM_Type_Int (l, r)
    | IntegerExpression i -> ((ir, scope), (string_of_int i, LLVM_Type_Int))
    | VarExpression v ->
      begin match (lookup scope v) with
        | Some (VariableSymbol (vt, vi)) ->
          let loaded_name = newtmp () in
          let load_instr = Load {
              load_var_name = make_var_name v;
              load_res_name = loaded_name;
              load_type = llvm_type_of_asd_type vt
            } in
          ((append_instr ir load_instr, scope), (loaded_name, LLVM_Type_Int))
        | _ -> failwith ("Use of undeclared variable '" ^ v ^ "'")
      end
    | ExprFunctionCall (id, args) ->
      begin match (lookup scope id) with
        | Some (FunctionSymbol f) ->
          if f.return_type = Type_Void then
            failwith "Attempt to call void function inside of an expression"
          else
            let (new_ir, _), arg_types_names =
              ir_of_function_arguments (ir, scope) args
            and res = newtmp () in
            let call_instr = Call {
                call_func_name = f.identifier;
                call_func_type = llvm_type_of_asd_type f.return_type;
                call_func_args = arg_types_names;
                call_func_store_ret = res
              } in
            (append_instr new_ir call_instr, scope), (res, LLVM_Type_Int)
        | _ -> failwith ("Call to undeclared function '" ^ id ^ "'")
      end
    | ArrayElemExpr (id, index_expr) ->
      begin match (lookup scope id) with
        | Some (VariableSymbol (vt, vi)) ->
          begin match vt with
            | Type_Array (stored_type, size) ->
                let (ref_ir, ref_scope), address_res =
                  ir_of_array_reference (ir, scope) (stored_type, id, size, index_expr)
                in
                let res = newtmp () in
                let load_instr = Load {
                    load_var_name = address_res;
                    load_type = LLVM_Type_Int;
                    load_res_name = res
                  } in
                ((append_instr ref_ir load_instr, scope), (res, LLVM_Type_Int))
            | _ -> failwith ("Variable '" ^ id ^ "' is not subscriptable")
          end
        | _ -> failwith ("Use of undeclared array '" ^ id ^ "'")
      end

(* In addition to the other usual returns value it returns the list of types
 * and names of the variable in which the results of the expressions are
 * stored. *)
and ir_of_function_arguments (ir, scope) expression_list =
  let arguments_type_and_name, new_ir =
    List.fold_left
      (
        fun (type_name_l, arg_ir) e ->
          let (expr_ir, _), (expr_name, expr_type) = ir_of_expression (arg_ir, scope) e in
          ((expr_type, expr_name) :: type_name_l, expr_ir)
      )
      ([], ir)
      expression_list
  in
  ((new_ir, scope), List.rev arguments_type_and_name)

and ir_of_array_reference (ir, scope) (stored_type, id, size, index_expression) =
  let (expr_ir, expr_scope), (expr_name, expr_type)
    = ir_of_expression (ir, scope) index_expression in
  if expr_type <> LLVM_Type_Int then
    failwith "Array can only be accessed through an integer subscript"
  else
    let address_res = newtmp () in
    let array_access_instr = ArrayAccess {
        array_access_name = make_var_name id;
        array_access_size = size;
        array_access_type = llvm_type_of_asd_type stored_type;
        array_access_offset_name = expr_name;
        array_access_offset_type = expr_type;
        array_access_store_name = address_res
    }
    in
    ((append_instr expr_ir array_access_instr, expr_scope), address_res)

and ir_of_functional_elem_list (ir, scope) =
  List.fold_left ir_of_functional_elem (ir, scope)

and ir_of_functional_elem (ir, scope) = function
  | Proto (return_type, function_name, params) ->
    begin match (lookup scope function_name) with
      | Some (FunctionSymbol _) ->
        failwith (
          "Declaration of previously declared function '" ^ function_name ^ "'"
        )
      | _ -> fst (ir_of_proto (ir, scope) (return_type, function_name, params) false)
    end
  | Function ((return_type, function_name, params), body) ->
      begin match (lookup scope function_name) with
        | Some (FunctionSymbol fs) when fs.state = Defined ->
          failwith (
            "Definition of previously defined function '" ^ function_name ^ "'"
          )
        | _ -> ()
      end;
      let (proto_ir, proto_scope), fs
        = ir_of_proto (ir, scope) (return_type, function_name, params) true in
      let (new_ir, new_scope) = ir_of_statement (proto_ir, proto_scope) body in
      (* Make sure there is at least one return statement,
         as clang will complain if it is not the case *)
      let (return_ir, return_scope)
        = ir_of_statement
          (new_ir, new_scope)
          (Return (return_type, (IntegerExpression 0))) in
      (append_instr return_ir FuncDefEnd, add scope fs)

(* in addition to the usual instructions and code, it returns the symbol of
 * prototyped function. *)
and ir_of_proto (ir, scope) (return_type, function_name, params) is_definition =
  if is_definition then
    let def_instr = FuncDef {
        def_func_name = function_name;
        def_func_type = llvm_type_of_asd_type return_type;
        (* The name of the parameters are changed in the llvm definition
         * because their values can't be manipulated the same way as other
         * variable --- if %p is a parameter, an add i32 %p, i32 %p is not
         * possible. Their value will be retrieved in usual variable
         * to enable the statements inside the function to work without
         * any change in the name of the variable they make reference to.
         *)
        def_func_params
          = List.map (fun p -> (LLVM_Type_Int, p ^ "__param_")) params
      }
    and fs = begin
      match (lookup scope function_name) with
        | Some (FunctionSymbol fss) ->
          if fss.return_type <> return_type || (List.length fss.arguments) <> (List.length params) then
            failwith
              (
                "Type mismatch between the declaration and the definition of '" ^
                function_name
                ^ "'"
              )
          else
            {
              return_type = fss.return_type;
              identifier = fss.identifier;
              arguments = List.map (fun p -> VariableSymbol (Type_Int, p)) params;
              state = Defined
            }
        | _ -> {
            return_type = return_type;
            identifier = function_name;
            arguments = List.map (fun p -> VariableSymbol (Type_Int, p)) params;
            state = Defined
          }
    end in
    (* Here we create the instructions that will retrieve the value of the
     * parameter for the reason stated above.
     *)
    let local_func_ir =
      List.fold_left (fun curr_ir param ->
          let alloca_instr = Alloca {
              alloca_var_name = make_var_name param;
              alloca_type = LLVM_Type_Int
            }
          and store_instr = Assign {
              assign_var_name = make_var_name param;
              assign_var_type = LLVM_Type_Int;
              assign_rhs_name = make_var_name (param ^ "__param_")
            } in
          append_instr (append_instr curr_ir alloca_instr) store_instr)
        (append_instr ir def_instr)
        params in
    let func_scope = merge fs.arguments scope in
    (local_func_ir, add func_scope (FunctionSymbol fs)), (FunctionSymbol fs)
  else
    let fs = FunctionSymbol {
        return_type = return_type;
        identifier = function_name;
        arguments = List.map (fun p -> VariableSymbol (Type_Int, p)) params;
        state = Declared
      } in
    (ir, add scope fs), fs

and ir_of_statement (ir, scope) = function
  | Assignment (lhs, rhs_e) ->
    let (lvalue_ir, lvalue_scope), (lvalue_res, lvalue_type) =
      ir_of_lvalue (ir, scope) lhs
    in
    let (expr_ir, expr_scope), (expr_name, expr_type)
      = ir_of_expression (lvalue_ir, scope) rhs_e
    in
    if expr_type <> lvalue_type then
      failwith ("Mismatched types during assignment")
    else
    let assign_instr = Assign {
        assign_var_name = make_var_name lvalue_res;
        assign_var_type = expr_type;
        assign_rhs_name = expr_name
      }
    in
    (append_instr expr_ir assign_instr, expr_scope)
  | Block (d, ss) ->
    let (new_ir, new_scope) = ir_of_declaration (ir, scope) d in
    let (block_ir, _) = List.fold_left ir_of_statement (new_ir, new_scope) ss in
    (block_ir, scope)
  | IfThenElse (e, t, opt_f) ->
    let c = { cond_expr = e;
              if_true = "";
              if_false = "" } in
    begin match opt_f with
    | None -> ir_of_if_then (ir, scope) c t
    | Some f -> ir_of_if_then_else (ir, scope) c t f
    end
  | While (e, s) ->
    let c = { cond_expr = e;
              if_true = "";
              if_false = "" } in
    ir_of_while (ir, scope) c s
  | Return (func_ret_type, e) ->
    let ret_instr, new_ir =
      begin match func_ret_type with
        | Type_Void -> (Return {
            ret_type = LLVM_Type_Void;
            ret_value = ""
          }, ir)
        | _ ->
          let (expr_ir, _), (expr_name, expr_type) = ir_of_expression (ir, scope) e in
          (Return {
              ret_type = expr_type;
              ret_value = expr_name
            }, expr_ir)
      end in
    (append_instr new_ir ret_instr, scope)
  | FunctionCall (id, args) ->
    begin match (lookup scope id) with
      | Some (FunctionSymbol f) ->
        if f.return_type = Type_Int then
          failwith "Attempt to call non-void function without storing its return value"
        else
          let (new_ir, _), arg_types_names =
            ir_of_function_arguments (ir, scope) args
          in
          let call_instr = Call {
              call_func_name = f.identifier;
              call_func_type = llvm_type_of_asd_type f.return_type;
              call_func_args = arg_types_names;
              call_func_store_ret = ""
            } in
          (append_instr new_ir call_instr, scope)
      | _ -> failwith ("Call to undeclared function '" ^ id ^ "'")
    end
  | Print args ->
    let format_string, reversed_expression_list =
      List.fold_left
          (
            fun (str, expr_list) arg ->
              match arg with
              | PrintText t -> (str ^ t, expr_list)
              | PrintExpression e -> (str ^ "%d", e :: expr_list)
          )
          ("", [])
          args
    and format_global = newglob ".fmt" in
    let format_llvm_str, format_str_size = string_transform format_string in
    let format_decl_instr = FormatDecl {
        format_decl_name = format_global;
        format_decl_str = format_llvm_str;
        format_decl_str_size = format_str_size
      } in
    let (new_ir, _), arg_types_names =
      ir_of_function_arguments (ir, scope) (List.rev reversed_expression_list)
    in
    let printf_instr = Printf {
        printf_format_name = format_global;
        printf_format_size = format_str_size;
        printf_args = arg_types_names
      } in
    (append_instr (append_header_instr new_ir format_decl_instr) printf_instr, scope)
  | Read args ->
    let format_string = String.trim (List.fold_left (fun str _ -> str ^ " %d") "" args)
    and format_global = newglob ".fmt" in
    let format_llvm_str, format_str_size = string_transform format_string in
    let format_decl_instr = FormatDecl {
        format_decl_name = format_global;
        format_decl_str = format_llvm_str;
        format_decl_str_size = format_str_size
      } in
    let arg_types_names, new_ir =
      List.fold_left
        (
          fun (curr_types_names, curr_ir) x ->
            let (lvalue_ir, lvalue_scope), (lvalue_res, lvalue_type) =
              ir_of_lvalue (curr_ir, scope) x
            in
            ((lvalue_type, lvalue_res) :: curr_types_names, lvalue_ir)
        )
        ([], ir)
        args
    in
    let scanf_instr = Scanf {
        scanf_format_name = format_global;
        scanf_format_size = format_str_size;
        scanf_args = List.rev arg_types_names
      } in
    (append_instr (append_header_instr new_ir format_decl_instr) scanf_instr, scope)

(* In addition to the usual instructions and scope, it returns a reference to
 * the lvalue and its type. *)
and ir_of_lvalue (ir, scope) lvalue =
  match lvalue with
    | LValueIdent id ->
      begin match (lookup scope id) with
        | Some (VariableSymbol (t, _)) ->
          if t <> Type_Int then
            failwith ("'" ^ id ^ "' is not an lvalue of type int")
          else
            (ir, scope), (id, llvm_type_of_asd_type t)

        | _ -> failwith ("Use of undeclared identifier '" ^ id ^ "'")
      end
    | LValueArrayElem (id, index_expr) ->
      begin match (lookup scope id) with
        | Some (VariableSymbol (t, _)) ->
          begin match t with
            | Type_Array (ty, n) ->
              let array_type = llvm_type_of_asd_type ty in
              let (ref_ir, ref_scope), res =
                ir_of_array_reference (ir, scope) (ty, id, n, index_expr)
              in
              (ref_ir, scope), (res, array_type)
            | _ -> failwith ("Variable '" ^ id ^ "' is not subscriptable")
          end
        | _ -> failwith ("Use of undeclared identifier '" ^ id ^ "'")
      end

and ir_of_declaration (ir, scope) =
  List.fold_left ir_of_declaration_elem (ir, scope)
and ir_of_declaration_elem (ir, scope) = function
  | VariableDeclaration i ->
    let new_scope = add scope (VariableSymbol (Type_Int, i))
    and decl_instr = Alloca {
        alloca_var_name = make_var_name i;
        alloca_type = LLVM_Type_Int
      } in
    (append_instr ir decl_instr, new_scope)
  | ArrayDeclaration (i, n) ->
    let new_scope = add scope (VariableSymbol (Type_Array (Type_Int, n), i))
    and decl_instr = ArrayAlloca {
        array_alloca_var_name = make_var_name i;
        array_alloca_size = n;
        array_alloca_type = LLVM_Type_Int
      } in
    (append_instr ir decl_instr, new_scope)

and ir_of_cond (ir, scope) c =
  let (expr_ir, expr_scope), (expr_result, expr_type) =
    ir_of_expression (ir, scope) c.cond_expr
  and res = newtmp () in
  let branch = Branch {
      cond_variable = res;
      true_label = c.if_true;
      false_label = c.if_false;
    }
  and cmp = Ifnz {
      ifnz_var_name = expr_result;
      ifnz_type = LLVM_Type_Int;
      ifnz_lval_name = res;
    } in
  let code = append_instr (append_instr expr_ir cmp) branch in
  (code, scope)

and ir_of_if_then (ir, scope) c t =
  let thenL = newlab "then"
  and endL = newlab "endif"
  in c.if_true <- thenL; c.if_false <- endL;
  let (cond_ir, cond_scope) = ir_of_cond (ir, scope) c in
  let (t_ir, t_scope) =
    ir_of_statement (append_instr cond_ir (Label thenL), cond_scope) t in
  (append_instr (append_instr t_ir (Jump endL)) (Label endL), scope)

and ir_of_if_then_else (ir, scope) c t f =
  let thenL = newlab "then"
  and elseL = newlab "else"
  and endL = newlab "endif"
  in c.if_true <- thenL; c.if_false <- elseL;
  let (cond_ir, cond_scope) = ir_of_cond (ir, scope) c in
  let (t_ir, t_scope) =
    ir_of_statement (append_instr cond_ir (Label thenL), cond_scope) t in
  let (f_ir, f_scope) =
    ir_of_statement (append_instr (append_instr t_ir (Jump endL)) (Label elseL), cond_scope) f in
  (append_instr (append_instr f_ir (Jump endL)) (Label endL), scope)

and ir_of_while (ir, scope) c s =
  let loopL = newlab "while"
  and doL = newlab "do"
  and doneL = newlab "done" in
  c.if_true <- doL; c.if_false <- doneL;
  let (cond_ir, cond_scope) = ir_of_cond (append_instr (append_instr ir (Jump loopL)) (Label loopL), scope) c in
  let (stat_ir, stat_scope) =
    ir_of_statement (append_instr cond_ir (Label doL), cond_scope) s in
  (append_instr
     (append_instr stat_ir (Jump loopL))
     (Label doneL), scope)
