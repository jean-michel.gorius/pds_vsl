(* TODO : extend when you extend the language *)

type ident = string

type expression =
  | AddExpression of expression * expression
  | SubstExpression of expression * expression
  | MulExpression of expression * expression
  | DivExpression of expression * expression
  | IntegerExpression of int
  | VarExpression of ident
  | ExprFunctionCall of ident * expression list
  | ArrayElemExpr of ident * expression

type typ =
  | Type_Int
  | Type_Void
  | Type_Array of typ * int

type cond = {
  cond_expr : expression;
  mutable if_true : string;
  mutable if_false : string
}

type lvalue =
  | LValueIdent of ident
  | LValueArrayElem of ident * expression

type declaration_elem =
  | VariableDeclaration of ident
  | ArrayDeclaration of ident * int

type declaration = declaration_elem list

type print_item =
  | PrintText of string
  | PrintExpression of expression

type read_item = lvalue

type statement =
  | Assignment of lvalue * expression
  | Block of declaration * statement list
  | IfThenElse of expression * statement * statement option
  | While of expression * statement
  | Return of typ * expression
  | FunctionCall of ident * expression list
  | Print of print_item list
  | Read of read_item list

type param = ident
type proto = typ * ident * param list

type functional_elem =
  | Proto of proto
  | Function of proto * statement

type program = functional_elem list
