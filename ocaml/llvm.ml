(* TODO : extend when you extend the language *)

(* This file contains a simple LLVM IR representation *)
(* and methods to generate its string representation  *)

open Utils

type llvm_type =
  | LLVM_Type_Int
  | LLVM_Type_Void

(* Warning: because of type inference, we can not
 * have the same field name in two record type
 * (actually this is possible but cumbersome)
 *)

(* fields in each instruction *)
type llvm_binop = {
  lvalue_name: string;
  lvalue_type: llvm_type;
  op: string;
  left: string;
  right: string;
}
and llvm_assign = {
  assign_var_name : string;
  assign_var_type : llvm_type;
  assign_rhs_name : string
}
and llvm_alloca = {
  alloca_var_name : string;
  alloca_type : llvm_type
}
and llvm_load = {
  load_var_name : string;
  load_res_name : string;
  load_type : llvm_type
}
and llvm_branch = {
  cond_variable : string;
  true_label : string;
  false_label : string
}
and llvm_ifnz = {
  ifnz_type : llvm_type;
  ifnz_var_name : string;
  ifnz_lval_name : string
}
and llvm_func_def = {
  def_func_name : string;
  def_func_type : llvm_type;
  def_func_params : (llvm_type * string) list
}
and llvm_call = {
  call_func_name : string;
  call_func_type : llvm_type;
  call_func_args : (llvm_type * string) list;
  call_func_store_ret : string
}
and llvm_format_decl = {
  format_decl_name : string;
  format_decl_str : string;
  format_decl_str_size : int
}
and llvm_printf = {
  printf_format_name : string;
  printf_format_size : int;
  printf_args : (llvm_type * string) list
}
and llvm_scanf = {
  scanf_format_name : string;
  scanf_format_size : int;
  scanf_args : (llvm_type * string) list
}
and llvm_array_access = {
  array_access_name : string;
  array_access_size : int;
  array_access_type : llvm_type;
  array_access_offset_name : string;
  array_access_offset_type : llvm_type;
  array_access_store_name : string
}
and llvm_array_alloca = {
  array_alloca_var_name : string;
  array_alloca_size : int;
  array_alloca_type : llvm_type
}
and llvm_return = {
  ret_type: llvm_type;
  ret_value: string; (* we only return identifier, or integers as strings made
                        of numerical character *)
}

(* instructions sum type *)
and llvm_instr =
  | Binop of llvm_binop
  | Assign of llvm_assign
  | Return of llvm_return
  | Alloca of llvm_alloca
  | Load of llvm_load
  | Branch of llvm_branch
  | Ifnz of llvm_ifnz
  | Label of string
  | Jump of string
  | FuncDef of llvm_func_def
  | FuncDefEnd
  | Call of llvm_call
  | FormatDecl of llvm_format_decl
  | Printf of llvm_printf
  | Scanf of llvm_scanf
  | ArrayAccess of llvm_array_access
  | ArrayAlloca of llvm_array_alloca

(* Note: instructions in list are taken in reverse order in
 * string_of_ir in order to permit easy list construction!!
 *)
and llvm_ir = {
  header: llvm_instr list; (* to be placed before all code (global definitions) *)
  code: llvm_instr list;
}

(* handy *)
let empty_ir = {
  header = [];
  code = [];
}

let append_instr ir code =
  { header = ir.header; code = code :: ir.code }
and append_header_instr ir code =
  { header = code :: ir.header; code = ir.code }

(* actual IR generation *)
let rec string_of_llvm_type = function
  | LLVM_Type_Int -> "i32"
  | LLVM_Type_Void -> "void"

and string_of_ir ir =
  (string_of_instr_list ir.header) ^ "\n\n" ^ (string_of_instr_list ir.code)

and string_of_instr_list l =
  String.concat "" (List.rev_map string_of_instr l)

and string_of_argument_list a =
  String.concat
    ", "
    (List.map (fun (t, v) -> string_of_llvm_type t ^ " " ^ make_var_name v) a)

and string_of_pointer_argument_list a =
  String.concat ", "
    (List.map (fun (t, v) -> string_of_llvm_type t ^ "* " ^ make_var_name v) a)

and string_of_instr = function
  | Binop v -> v.lvalue_name ^ " = " ^
               v.op ^ " " ^ (string_of_llvm_type v.lvalue_type) ^ " " ^ v.left ^ ", " ^
               v.right ^ "\n"
  | Assign a -> "store " ^
                string_of_llvm_type a.assign_var_type ^ " " ^
                a.assign_rhs_name ^ ", " ^
                string_of_llvm_type a.assign_var_type ^ "* " ^
                a.assign_var_name ^ "\n"
  | Alloca a -> a.alloca_var_name ^ " = alloca " ^ string_of_llvm_type a.alloca_type ^ "\n"
  | Load l -> l.load_res_name ^ " = load " ^ string_of_llvm_type l.load_type ^ ", " ^
              string_of_llvm_type l.load_type ^ "* " ^ l.load_var_name ^ "\n"
  | Branch b -> "br i1 " ^ b.cond_variable ^ ", label %" ^ b.true_label ^
                ", label %" ^ b.false_label ^ "\n"
  | Ifnz i -> i.ifnz_lval_name ^ " = icmp ne " ^ string_of_llvm_type i.ifnz_type ^ " " ^
              i.ifnz_var_name ^ ", 0\n"
  | Label l -> l ^ ":\n"
  | Jump j -> "br label %" ^ j ^ "\n"
  | FuncDef fd -> "define " ^ string_of_llvm_type fd.def_func_type ^ " @" ^
                  fd.def_func_name ^
                  "(" ^
                  string_of_argument_list fd.def_func_params ^
                  ")\n{\n"
  | FuncDefEnd -> "}\n\n"
  | Call c -> (
               (* add an equal sign if there is a variable in which to store
                * the value returned by the function call *)
               if c.call_func_store_ret = "" then ""
               else c.call_func_store_ret ^ " = "
              ) ^
              "call " ^ string_of_llvm_type c.call_func_type ^ " @" ^ c.call_func_name ^ "(" ^
               string_of_argument_list c.call_func_args ^ ")\n"
  | FormatDecl f -> f.format_decl_name ^ " = global [" ^ string_of_int f.format_decl_str_size ^
                    " x i8] c\"" ^ f.format_decl_str ^ "\"\n"
  | Printf p -> let sz = string_of_int p.printf_format_size in
    "call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([" ^ sz ^ " x i8], [" ^
    sz ^ " x i8]* " ^ p.printf_format_name ^ ", i64 0, i64 0)" ^
    (if p.printf_args = [] then "" else ", ") ^
    string_of_argument_list p.printf_args ^
    ")\n"
  | Scanf s -> let sz = string_of_int s.scanf_format_size in
    "call i32 (i8*, ...) @scanf(i8* getelementptr inbounds ([" ^ sz ^ " x i8], [" ^
    sz ^ " x i8]* " ^ s.scanf_format_name ^ ", i64 0, i64 0)" ^
    (if s.scanf_args = [] then "" else ", ") ^
    string_of_pointer_argument_list s.scanf_args ^
    ")\n"
  | ArrayAccess a ->
    let sz = string_of_int a.array_access_size
    and t = string_of_llvm_type a.array_access_type in
    a.array_access_store_name ^ " = getelementptr [" ^ sz ^ " x " ^ t ^ "], [" ^
    sz ^ " x " ^ t ^ "]* " ^ a.array_access_name ^ ", i64 0, " ^
    string_of_llvm_type a.array_access_offset_type ^ " " ^ a.array_access_offset_name ^ "\n"
  | ArrayAlloca a -> a.array_alloca_var_name ^ " = alloca [" ^ string_of_int a.array_alloca_size ^
                     " x " ^ string_of_llvm_type a.array_alloca_type ^ "]\n"
  | Return v ->
      "ret " ^ (string_of_llvm_type v.ret_type) ^ " " ^ v.ret_value ^ "\n"
